#!/usr/bin/python
# -*- coding: utf-8 -*-
from Adafruit_CharLCD import Adafruit_CharLCD
from subprocess import *
import RPi.GPIO as GPIO
import os
import time
import subprocess
import sys
from mpd import MPDClient
from pulse import PeakMonitor
debug = "true"
INT_1 = 12
INT_2 = 16
INT_3 = 20
INT_4 = 21
#KEY_PLAY = 19	
#$KEY_STOP = 13	
#KEY_NEXT = 26	
#KEY_PREV = 6	
#KEY_MODE = 5	
#KEY_MUTE = 11	
#KEY_VOLUP = 22
#KEY_VOLDOWN = 27
KEY_2 = 22    
KEY_3 = 11     
KEY_1 = 27     
KEY_4 = 5   
KEY_5 = 6     
KEY_6 = 13   
KEY_7 = 19
KEY_8 = 26
METER_RATE = 8
MAX_SAMPLE_VALUE = 127
DISPLAY_SCALE = 2
lcd = Adafruit_CharLCD()
lcd.begin(20,2)
argument = ""
try:
	argument = sys.argv[1]
except:
	pass 
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(INT_1, GPIO.OUT)
GPIO.setup(INT_2, GPIO.OUT)
GPIO.setup(INT_3, GPIO.OUT)
GPIO.setup(INT_4, GPIO.OUT)
#Запуск и остановка демона
if "stop" in argument:
	lcd.clear()
       	lcd.setCursor(0,0)
       	text = "Goodbay"
       	maximum = 20
       	text = ' ' * maximum + text
       	for i in range(14):
               	lcd.setCursor(0,0)
               	lcd.message(u"{: <20}".format(text[i:i + maximum]))
               	time.sleep(0.4)
       	time.sleep(2)
	lcd.clear()
	#GPIO.output(INT_1, True)
        #GPIO.output(INT_2, True)
	exit()
elif "start" in argument:
	lcd.clear()
        lcd.setCursor(0,0)
        text = "Raspberry Audio"
        maximum = 20
        text = ' ' * maximum + text
        for i in range(18):
                lcd.setCursor(0,0)
                lcd.message(u"{: <20}".format(text[i:i + maximum]))
                time.sleep(0.4)
        time.sleep(0.5)
SINK_NAME = 'alsa_output.0.analog-stereo'
MAX_SPACES = int( MAX_SAMPLE_VALUE >> DISPLAY_SCALE )
monitor = PeakMonitor(SINK_NAME, METER_RATE)
lcd.clear()

GPIO.setup(KEY_2, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(KEY_3, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(KEY_1, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(KEY_4, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(KEY_5, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(KEY_6, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(KEY_7, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(KEY_8, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.output(INT_1, True)
GPIO.output(INT_2, True)
GPIO.output(INT_3, True)
GPIO.output(INT_4, True)
#Создание собственных символов
sym = [0b10000, 0b11000, 0b11100, 0b11110, 0b11100, 0b11000, 0b10000, 0b00000]
lcd.createChar(1, sym)
sym = [0b00000, 0b11011, 0b11011, 0b11011, 0b11011, 0b11011, 0b11011, 0b00000]
lcd.createChar(2, sym)
sym = [0b00000, 0b11111, 0b11111, 0b11111, 0b11111, 0b11111, 0b11111, 0b00000]
lcd.createChar(3, sym)
global a
global mode
mode = 3
a = 0
client = MPDClient()               # create client object
client.timeout = 6                # network timeout in seconds (floats allowed$
client.idletimeout = 20          # timeout for fetching the result of the idl$
while 1:
        try:
                client.connect("localhost", 6600)  # connect to localhost:
                GPIO.output(INT_1, False)
                GPIO.output(INT_2, False)
                break
        except:
                pass
cmd = "ip addr show eth0 | grep inet | awk '{print $2}' | cut -d/ -f1"

def run_cmd(cmd):
    p = Popen(cmd, shell=True, stdout=PIPE)
    output = p.communicate()[0]
    return output

def mpc_text():
	while 1:
   		try:
       			st = client.status()
			so = client.currentsong()
			break
  		except:
			try:
                		client.disconnect()
                		client.connect("localhost", 6600)
			except:
				pass
	if 'file' in so:
               	file = so['file']
        else:
                file = "none"
        if 'artist' in so:
                artist = so["artist"]
        else:
                artist = "none"
        if 'title' in so:
                title = so["title"]
        else:
                title = "none"
        if 'state' in st:
                mstate = st["state"]
        else:
                mstate = "stop"
        if 'volume' in st:
                global vol
                vol = st['volume']
        else:
                vol = 0
	if str(file[0:4]) == "http":
		text = unicode (' ' * maximum + title+" - "+ artist,"utf-8")
	else:
		text = unicode (' ' * maximum + file,"utf-8")
	return {'file':file, 'artist':artist, 'title':title, 'state':mstate, 'volume':vol, 'text':text }
#Установка текста в середине экрана
def center_lcd(msg):
	string = ' ' * (10 - len(msg)//2) + msg + ' ' * (20-(10-len(msg)//2)+len(msg)) 
	return string
#Функции описания клавиш
def func_play(chanel):
        if mode == 1:
                global a
                if(a == 0):
                        a = 1
                        run_cmd("mpc play >> /dev/null")
                else:
                        a = 0
                        run_cmd("mpc pause >> /dev/null")
def func_stop(chanel):
	if mode == 1:
		global a
		a = 0
        	run_cmd("mpc stop >> /dev/null")
        	lcd.setCursor(0,1)
        	lcd.write4bits(3, True)
def func_prev(chanel):
	if mode == 1:
		run_cmd("mpc prev >> /dev/null")
def func_next(chanel):
	if mode == 1:
		run_cmd("mpc next >> /dev/null")
	elif mode == 2:
		run_cmd("service networking restart")		
def func_vol_up(chanel):
	if mode == 3:
		cmd = "pactl set-sink-volume 0 -- +5%"
		run_cmd(cmd)
	elif mode == 1:
		run_cmd("mpc volume +10 >> /dev/null")
def func_vol_down(chanel):
	if mode == 3:
		cmd = "pactl set-sink-volume 0 -- -5%"
		run_cmd(cmd)
	elif mode == 1:		
		run_cmd("mpc volume -10 >> /dev/null")
def func_mode(chanel):
	global mode
	mode = mode + 1
	if mode > 3:
		mode = 1
#Блок прерываний
GPIO.add_event_detect(KEY_2, GPIO.RISING, callback=func_play, bouncetime=1000)
GPIO.add_event_detect(KEY_3, GPIO.RISING, callback=func_stop, bouncetime=1000)
GPIO.add_event_detect(KEY_1, GPIO.RISING, callback=func_next, bouncetime=1000)
GPIO.add_event_detect(KEY_4, GPIO.RISING, callback=func_prev, bouncetime=1000)
GPIO.add_event_detect(KEY_7, GPIO.RISING, callback=func_vol_up, bouncetime=1000)
GPIO.add_event_detect(KEY_8, GPIO.RISING, callback=func_vol_down, bouncetime=1000)
GPIO.add_event_detect(KEY_6, GPIO.RISING, callback=func_mode, bouncetime=1000)
maximum = 20
run_once = [True,True,True]
while 1:
 if mode == 1:
	if run_once[0]:
		run_once = [False,True,True]
		lcd.clear()
	run_once[1] = True
	run_once[2] = True
	data = mpc_text()
	lcd.setCursor(0,0)
	lcd.message(" "*20)
	lcd.setCursor(0,0)
	try:
		lcd.message(unicode(data['title'][0:19],"utf-8"))
	except:
		lcd.message(unicode(data['title'],"utf-8"))
	time.sleep(4)
	lcd.setCursor(0,0)
        lcd.message(" "*20)
	text = data["text"]
	old = {"state": data["state"],"volume": data["volume"],"text": data["text"]}
        for i in range(len(text)):
		if mode <> 1:
			break
		data = mpc_text()
		if old["text"] == data["text"]:
			lcd.setCursor(0,0)
			lcd.message(u"{: <20}".format(text[i:i + maximum]))			
		else:
			break
		if old["state"] == data["state"] or old["volume"] == data["volume"]:
                	lcd.setCursor(0,1)
			if data["state"] == "play":
				sym = 1
			elif data["state"] == "pause":
				sym = 2
			elif data["state"] == "stop":
				sym = 3
			else:
				sym = 0x00
			lcd.write4bits(sym, True)
			lcd.setCursor(1,1)
			lcd.message("   ")
			lcd.setCursor(4,1)
			lcd.message(u"{:-<10}".format(int(data["volume"])//10* "\b"))
			lcd.setCursor(15,1)
			lcd.message(u"{: ^3}".format(data["volume"])+ "% ")
		else:
			old = {"state": data["state"],"volume": data["volume"]}
			break
		time.sleep(0.4)
 elif mode == 2:
	if run_once[1]:
		lcd.clear()
		run_once = [True,False,True] 
 
	lcd.setCursor(0,0)
	lcd.message(center_lcd("IP address mode"))
	ipaddr = run_cmd(cmd)
	lcd.setCursor(0,1)
	lcd.message(center_lcd(ipaddr))	
	time.sleep(0.5)
 elif mode == 3:
	run_once = [True,True,False]
        lcd.clear()
	lcd.setCursor(0,0)
	lcd.message(center_lcd("Pulse Meter"))		
	for sample in monitor:
		if not mode == 3:
			break
		lcd.setCursor(0,1)
		sample = int((sample >> DISPLAY_SCALE)*0.7)
        	spaces = ' ' * (20 - sample)
		if sample > 20:
			sample = 20
		lcd.message('%s%s' % ('\b'*sample, spaces))
		sys.stdout.flush()
		
GPIO.cleanup()
